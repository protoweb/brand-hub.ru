$(document).ready(function () {
	$('select').niceSelect();
	$(".js-range-slider").ionRangeSlider();

	function checkValue(input) {
		return input.prop('checked') !== true;
	}

	$('.custom_checkbox_item').on('click', function (e){
		e.preventDefault();
		const input = $(this).find('input');
		input.prop('checked', checkValue(input))

		if (input.prop('checked') === true) {
			$(this).find('button').removeClass('button_white').addClass('button_blue').find('span').text('Выбрано');
		} else {
			$(this).find('button').removeClass('button_blue').addClass('button_white').find('span').text('Выбрать')
		}
	});

	$('.select_one_block_item').on('click', function (e) {
		e.preventDefault();
		const buttons = $(this).closest('.select_one_block').find('button');
		const inputs = $(this).closest('.select_one_block').find('input');
		inputs.prop('checked', false);
		buttons.removeClass('button_blue').addClass('button_white').find('span').text('Выбрать');

		$(this).find('button').removeClass('button_white').addClass('button_blue').find('span').text('Выбрано');
		$(this).find('input').prop('checked', true)

	});

	$('.consumer_behavior_buttons button').on('click', function (e) {
		e.preventDefault();
		$('.consumer_behavior_buttons button').removeClass('active');
		$(this).addClass('active')
		const data = $(this).attr('data');
		const value = $(this).attr('value');
		$('#consumer_behavior_image img').attr('src', `pic/${data}.png`);
		$('#consumer_input').attr('value', value)
	});

	$('button.information_drop_button').on('click', function (e) {
		e.preventDefault();
		$(this).closest('.information_drop').find('.information_overlay').fadeIn();
	});

	$('button.information_drop_button_head').on('click', function (e) {
		e.preventDefault();
		$(this).closest('.information_drop').find('.information_overlay_head').fadeIn();
	});

	const pr_status_popup = $('.information_overlay, .information_overlay_head');

	$(document).mouseup(function (e) {
		if (!pr_status_popup.is(e.target)
			&& pr_status_popup.has(e.target).length === 0)
		{
			$('.information_overlay, .information_overlay_head').fadeOut();
		}
	});

});
