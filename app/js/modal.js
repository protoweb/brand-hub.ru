$(document).ready(function () {

	const body = $('body');

	// MODAL "В шаге от своего бренда"
	$('button#login_button').on('click', function () {
		closeModal($('.modal'));
		makeModal($('#modal_login_welcome'));
	});

	// MODAL Войти для моб навигации
	$('button#login_button_mobile').on('click', function () {
		$('.wrapped_block').removeClass('lock');
		closeModal($('.modal'));
		makeModal($('#modal_login_welcome'));
		$('#modal_nav_position > ul').find('li').toggleClass('open');
		$('#modal_nav_position').removeClass('service_active')
		$('ul.mobile_drop_nav').find('li').removeClass('open');
		$('#header').toggleClass('open');
		if (!$('#modal_nav').hasClass('active_modal')) {
			$('ul.mobile_drop_nav').hide();
			$('#modal_nav_position').removeClass('service_active');
		}
	})

	// ОТКРЫТЬ УСЛУГИ В МОБ МЕНЮ
	$('#mobile_nav_service_button').on('click', function () {
		$(this).closest('li').find('ul').slideToggle();
		$('#modal_nav_position').toggleClass('service_active')
	});

	// Открывать моб меню
	$('button#mobile_nav_button').on('click', function () {
		const $scrollableElement = document.querySelector('.wrapped_block');
		const modal_nav = $('#modal_nav');
		modal_nav.toggleClass('active_modal');
		if (!$('#modal_nav').hasClass('active_modal')) {
			$('ul.mobile_drop_nav').hide();
			$('#modal_nav_position').removeClass('service_active');
		}
		$(this).toggleClass('active');
		$('button.close_main_nav').toggleClass('active');
		$('#modal_nav').find('li').toggleClass('open');
		$('ul.mobile_drop_nav').find('li').removeClass('open');
		$('#header').toggleClass('open');
		if (modal_nav.hasClass('active_modal')) {
			$('.wrapped_block').addClass('lock');
		} else {
			$('.wrapped_block').removeClass('lock');
		}
	});

	// MODAL ФОРМА РЕГИСТРАЦИИ
	$('button#modal_sub_reg_button, button#modal_registration_button').on('click', function () {
		closeModal($('.modal'));
		makeModal($('#modal_registration'))
	});

	$('button#modal_login_button').on('click', function () {
		closeModal($('.modal'));
		makeModal($('#modal_login'))
	});

	$('button#modal_sub_login_button').on('click', function () {
		closeModal($('.modal'));
		makeModal($('#modal_login'))
	});

	// ВОССТАНОВЛЕНИЕ ПАРОЛЯ
	$('button#restore_password_button').on('click', function (e) {
		e.preventDefault();
		closeModal($('.modal'));
		makeModal($('#modal_restore_password'))
	});

	// Открыть окно модалки с видео
	$('button#main_video_popup, button#close_modal_video, button#simple_video').on('click', function () {
		$('#modal_video_block').toggleClass('active_modal');
	});

	document.addEventListener('keyup', function (e) {
		if (e.key === 'Escape') {
			closeModal($('.modal'));
		}
	})

	$('button#service_questions_button').on('click', function () {
		$('#service_questions_block').toggleClass('active');
		const service_question_content = $('.service_questions_content');
		service_question_content.animate({
			scrollTop: service_question_content.offset().top
		}, 0);
	})


	// MODAL FUNCTIONS

	$('button.close_modal').on('click', function () {
		closeModal($(this).closest('.modal'));
		$('#service_questions_block').removeClass('active');
		$('.modal_overlay').remove();
	});

	body.on('click', '.modal_overlay', function () {
		closeModal($('.modal'));
		$('.modal_overlay').remove();
	});

	function makeModal(modal_item) {
		const width = $(document).width();
		$(modal_item).addClass('active_modal');
		if (width > 700) body.append('<div class="modal_overlay"></div>');
		scrollLock.enablePageScroll('body');
	}

	function closeModal(modal_item) {
		$('button#mobile_nav_button').removeClass('active');
		$(modal_item).removeClass('active_modal');
		$('.modal_overlay').remove();
		scrollLock.enablePageScroll('body');
	}

});
