$(document).ready(function () {

	// Калькулятор Кастом заказа
	let additional_services_array = []; // Дополнительные услуги

	let logo_price = 0; // Цена логотипа
	let logo_count = 0; // Кол-во логотипов
	const logo_item = $('#logo_item');
	const logo_count_input = $('#logos_count'); // input счетчика логотипов

	let package_price = 0; // Цена упаковки
	let package_count = 0; // Кол-во упаковок
	const package_item = $('#package_item');
	const package_count_input = $('#package_count') // input счетчика упаковок

	const items_total_price_id = $('#bc_block_total');
	let items_total_price = 0;

	const basic_logo = $('#basic_logo');
	const basic_package = $('#basic_package');

	const add_button = $('button.add'); // Кнопка плюс
	const sub_button = $('button.sub'); // Кнопка минус

	// Маска для цены
	function priceMask(price) {
		const str_price = price.toString();
		switch (str_price.length) {
			case 5: {
				const first_dig = [str_price.substring(0, 2)];
				first_dig.push(' ');
				return first_dig.concat(str_price.substring(2, str_price.length)).join('')
			}
			case 6: {
				const first_dig = [str_price.substring(0, 3)];
				first_dig.push(' ');
				return first_dig.concat(str_price.substring(3, str_price.length)).join('')
			}
			case 7: {
				const first_dig = [str_price.substring(0, 1)];
				first_dig.push(' ');
				const second_dig = [str_price.substring(1, 4)];
				second_dig.push(' ');
				const join_array = first_dig.concat(second_dig);
				const last_dig = [str_price.substring(4, 7)];
				return join_array.concat(last_dig).join('')
			}
			default:
				return price;
		}
	}

	// Инициализация
	function initCalc() {
		logo_count_input.attr('value', logo_count)
		package_count_input.attr('value', package_count)

		// Инициализация цены логотипа
		$('#logo_price').text(priceMask(logo_item.attr('data-price')));
		logo_price = logo_item.attr('data-price');

		// Инициализация цены упаковки
		$('#package_price').text(priceMask(package_item.attr('data-price')));
		package_price = package_item.attr('data-price');

		$('#cart_total_price').text(0)

		// Инициализация Total цены блока Основа проекта
		items_total_price_id.text(items_total_price);
	}

	// Рендер блока Основа проекта
	function renderItems() {
		logo_count_input.attr('value', logo_count);
		package_count_input.attr('value', package_count);
		items_total_price_id.text(priceMask(logo_price * logo_count + package_price * package_count));

		if (logo_count > 0) {
			basic_logo.find($('.total_item')).remove()
			basic_logo.append(templateItems('logo'));
		}
		if (package_count > 0) {
			basic_package.find($('.total_item')).remove()
			basic_package.append(templateItems('package'));
		}
		if (logo_count === 0) basic_logo.find($('.total_item')).remove()
		if (package_count === 0) basic_package.find($('.total_item')).remove()

		renderTotal();
	}

	$(document).on('click', 'button.delete', function () {
		$('.total_item').removeClass('confirm');
		$(this).closest('.total_item').addClass('confirm');
	});

	$(document).on('click', 'button.cancel', function () {
		$(this).closest('.total_item').removeClass('confirm');
		renderItems()
	})

	$(document).on('click', 'button.delete_confirm', function () {
		const label = $(this).closest('.total_item').data('label');
		if (label === 'logo') {
			logo_count = 0;
			renderItems()
		} else {
			package_count = 0;
			renderItems()
		}
	})

	$(document).on('click', 'button.delete_confirm_service', function () {
		const id = $(this).data('id');
		additional_services_array = additional_services_array.filter((item) => item.id !== id);
		$(`.sc_checkbox_box:eq(${id})`).find('.sc_checkbox').removeClass('active');
		renderAdditionalService();
	});

	function renderTotal() {
		const reducer = (accumulator, currentValue) => accumulator + currentValue;
		let price_array = [logo_price * logo_count, package_price * package_count];
		const total_price_main_services = price_array.reduce(reducer);

		if (total_price_main_services > 0) {
			$('button#submit_custom').attr('disabled', false)
		} else {
			$('button#submit_custom').attr('disabled', true)
		}

		additional_services_array.forEach(item => {
			price_array.push(item.price)
		})
		price_array = price_array.reduce(reducer)




		$('#cart_total_price').text(priceMask(price_array)) // Полный прайс 2х услуг и доп
	}

	function renderAdditionalService() {
		let total = null;
		$('#additional_items_list').html('');
		$('#additional_services').html('');

		additional_services_array.forEach(item => {
			total += item.price;
			$('#additional_items_list').append(templateAdditionalService(item.id, item.title, item.price))
			$('#additional_services').append(templateAdditionalTotal(item.id, item.title, item.price))

		})
		$('#ad_block_total').text(total === null ? 0 : priceMask(total))
		renderTotal();
	}

	$('.sc_checkbox').on('click', function () {
		const id = $(this).closest('.sc_checkbox_box').index();
		if ($(this).hasClass('active')) {
			additional_services_array = additional_services_array.filter((item) => item.id !== id)
			$(this).removeClass('active')
		} else {
			const title = $(this).find('.title').text();
			const price = $(this).data('price').split(' ').join('');
			additional_services_array.push({id, title, price: parseInt(price)});
			$(this).addClass('active');
		}
		renderAdditionalService();
	})

	function calculateItems(label, operation) {
		switch (operation) {
			case 'plus': {
				if (label === 'logo') {
					if (logo_count === 0) basic_logo.append(templateItems('logo'));
					logo_count++
					$('#logo_calc_description').html(`Разработка <b>${logo_count}</b> ${selectStringOption(logo_count)}`);
				} else {
					if (package_count === 0) basic_package.append(templateItems('package'));
					package_count++
					$('#package_calc_description').html(`Разработка <b>${package_count}</b> ${selectStringOption(package_count)}`);
				}
				renderItems();
				break;
			}
			case 'minus': {
				if (label === 'logo') {
					if (logo_count !== 0) {
						logo_count--
						$('#logo_calc_description').html(`Разработка <b>${logo_count}</b> ${selectStringOption(logo_count)}`);
					}
					if (logo_count === 0) {
						basic_logo.find($('.total_item')).remove()
						$('#logo_calc_description').html('');
					}
				} else {
					if (package_count !== 0) {
						package_count--
						$('#package_calc_description').html(`Разработка <b>${package_count}</b> ${selectStringOption(package_count)}`);
					}
					if (package_count === 0) {
						basic_package.find($('.total_item')).remove();
						$('#package_calc_description').html('');
					}
				}
				renderItems();
				break;
			}
			default:
				return false;
		}
	}

	function selectStringOption(count) {
		const lastFigure = parseInt(count.toString().substr(count.toString().length - 1, 1));
		if (count >= 11 && count < 15) {
			return 'варианта';
		} else {
			if (lastFigure === 1) return 'вариант';
			if (lastFigure > 1 && lastFigure < 5) return 'варианта';
			if (lastFigure === 0 || lastFigure >= 5) return 'вариантов';
		}
	}

	function templateItems(title) {
		return (
			`<div class="total_item" data-label=${title === 'logo' ? 'logo' : 'package'}>
				<div class="title">${title === 'logo' ? 'Логотип' : 'Упаковка'}. Разработка ${title === 'logo' ? logo_count : package_count} ${selectStringOption(title === 'logo' ? logo_count : package_count)}</div>
				<div class="price_button">
					<div class="price price_icon">${title === 'logo' ? priceMask(logo_price * logo_count) : priceMask(package_price * package_count)}</div>
					<button class="delete"><img src="pic/delete-icon.svg" alt="Удалить"></button>
				</div>
				<div class="confirm_buttons">
					<button class="delete_confirm button_white">
						<span><img src="pic/confirm-delete.svg" alt="Удалить"/> Удалить</span>
					</button>
					<button class="cancel button_white">
						<span>Отменить</span>
					</button>
				</div>
			</div>`
		)
	}

	function templateAdditionalTotal(id, title, price) {
		return (
			`
			<div class="total_item">
				<div class="title">${title}</div>
				<div class="price_button">
					<div class="price price_icon">${priceMask(price)}</div>
					<button class="delete"><img src="pic/delete-icon.svg" alt="Удалить"></button>
				</div>
				<div class="confirm_buttons">
					<button data-id=${id} class="delete_confirm_service button_white">
						<span><img src="pic/confirm-delete.svg" alt="Удалить"/> Удалить</span>
					</button>
					<button class="cancel button_white">
						<span>Отменить</span>
					</button>
				</div>
			</div>
		`
		)
	}

	function templateAdditionalService(id, title, price) {
		return (
			`
			<div class="bc_block_total_item">
				<span>${title}</span>
				<span class="price_icon">${priceMask(price)}</span>
			</div>
			`
		)
	}

	add_button.on('click', function () {
		const take_label = $(this).closest('.basics_custom_block_item').data('item');
		calculateItems(take_label, 'plus')
	})

	sub_button.on('click', function () {
		const take_label = $(this).closest('.basics_custom_block_item').data('item');
		calculateItems(take_label, 'minus')
	})

	// Хелперы
	const sc_helper_modal = $('.sc_helper_modal');
	$(document).mouseup(function (e) {
		if (!sc_helper_modal.is(e.target)
			&& sc_helper_modal.has(e.target).length === 0) {
			$('.sc_helper_modal').removeClass('active');
		}
	});
	$('button.sc_helper_button').on('click', function () {
		$(this).closest('.sc_checkbox_box').find('.sc_helper_modal').toggleClass('active')
	});

	initCalc();

});
