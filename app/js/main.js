$(document).ready(function () {

	let rotate = 134.17;
	let value = $(window).scrollTop();

	$(window).scroll(function () {

		function makeRotate() {
			if (value > $(window).scrollTop()) {
				rotate = rotate - 3;
			} else {
				rotate = rotate + 3
			}
			value = $(window).scrollTop();
		}

		$('#slogan_box_round').css({
			background: `linear-gradient(${rotate}deg, #BBCEFF 4.98%, #1859FF 94.88%)`
		})

		makeRotate();

	});

	$('.main_infographics_block_item').viewportChecker({
		classToAdd: 'visible',
		classToAddForFullView: '',
		classToRemove: 'invisible',
		offset: 100,
	});

	function makeSlider(item) {
		const windowResizeSize = $('body').width();
		if (windowResizeSize < 700) {
			item.addClass('adaptive_slider owl-carousel');
			$('.adaptive_slider').owlCarousel({
				items: 1,
				loop: true,
				nav: false,
				dots: false
			});
		} else {
			$('.adaptive_slider').owlCarousel('destroy');
			item.removeClass('parters_slider owl-carousel');
		}
	}

	$('#services_footer_content_slider').owlCarousel({
		items: 3,
		loop: true,
		smartSpeed: 500,
		autoplay: true,
		dots: false,
		nav: false,
		margin: 25,
		responsive: {
			0: {
				items: 1,
			},
			601: {
				items: 3,
			}
		}
	});

	$('#services_audit').owlCarousel({
		items: 3,
		loop: true,
		smartSpeed: 500,
		autoplayTimeout: 7000,
		autoplay: true,
		dots: true,
		nav: false,
		margin: 25,
		responsive: {
			0: {
				items: 1,
				autoplay: false,
			},
			601: {
				items: 2,
				autoplay: false,
			},
			1024: {
				items: 3,
			}

		}
	})

	$('.two').owlCarousel({
		items: 2,
		loop: true,
		smartSpeed: 500,
		autoplay: true,
		dots: true,
		margin: 25,
		responsive: {
			0: {
				items: 1,
			},
			601: {
				items: 2,
			}
		}
	})

	$('.once').owlCarousel({
		items: 1,
		loop: true,
		smartSpeed: 500,
		autoplay: true,
		dots: true,
	})

	$('#full_width_slider').owlCarousel({
		items: 6,
		loop: true,
		autoplay: true,
		smartSpeed: 500,
		margin: 25,
		center: true,
		dots: false,
		nav: false,
		autoplayTimeout: 3000,
		autoplayHoverPause: true,
		responsive: {
			0: {
				items: 1,
			},
			1024: {
				items: 4,
			},
			1280: {
				items: 4,
			},
			1800: {
				items: 6,
			}
		}
	});

	$('button.password_toggle_icon').on('click', function (e) {
		e.preventDefault();
		const item = $(this).closest('.password_show_hide');
		item.toggleClass('active');
		if (item.hasClass('active')) {
			item.find('input').attr('type', 'text');
		} else {
			item.find('input').attr('type', 'password');
		}

	});

	// Слайдер на 2 главной странице
	const one_slider_with_nav = $('.one_slider_with_nav');
	one_slider_with_nav.owlCarousel({
		items: 1,
		nav: true,
		dotClass: 'owl-dot',
		dotsClass: 'owl-dots',
		autoplay: true,
		dots: true,
		loop: true,
		smartSpeed: 1000,
		autoplayTimeout: 3000,
		autoplayHoverPause: true,
		navText: [
			"<button class='owl_slider_prev'><span><img src='pic/slider-left.svg' alt=''></span></button>",
			"<button class='owl_slider_next'><span><img src='pic/slider-right.svg' alt=''></span></button>"
		],
	});

	const customers_talking_about_slider = $('.customers_talking_about_slider');
	let slider_index = '01';
	let slider_length = Math.floor($('.owl-item').length / 2);
	if (slider_length < 10) {
		slider_length = '0' + slider_length
	}
	$('#customers_talking_count_first').text(slider_index)

	one_slider_with_nav.owlCarousel();
	one_slider_with_nav.on('changed.owl.carousel', function (event) {
		const slider_index_item = event.page.index + 1;
		if (slider_index_item < 10) {
			slider_index = '0' + slider_index_item
		} else {
			slider_index = slider_index_item;
		}
		const slider_index_count = event.page.count
		if (slider_index_count < 10) {
			slider_length = '0' + slider_index_count
		} else {
			slider_length = slider_index_count;
		}
		$('#customers_talking_count_first').text(slider_index)
	})

	const slider_dots = customers_talking_about_slider.find('.owl-dots');
	const slider_nav = customers_talking_about_slider.find('.owl-nav');

	$('#customers_talking_count_last').append(slider_length)
	$('#customers_talking_dots').append(slider_dots);
	$('#customers_talking_buttons').append(slider_nav);

	// Конец слайдер на главной странице

	makeSlider($('#service_grid_main2').find('.services_grid_position'));

	// Табы
	$('button.stages_tab_item').on('click', function () {
		const stages_tab_content = $('.stages_tab_content');
		const index = $(this).index() + 1;
		$('button.stages_tab_item').removeClass('active');
		$(this).addClass('active');
		stages_tab_content.find('.stages_tab_content_block').removeClass('active');
		stages_tab_content.find(`.stages_tab_content_block:nth-child(${index})`).addClass('active');
		if (index === 2) {
			$('#stages_tab_line').find('span').addClass('next')
		} else {
			$('#stages_tab_line').find('span').removeClass('next')
		}

	});

});
